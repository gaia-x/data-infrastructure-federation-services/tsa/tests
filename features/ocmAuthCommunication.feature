As policy administrator
I want to evaluate OCM communication feature
So I am able to create policies to control login functionality

Scenario: Make request for QR code and presentationID
Given the policy to query OCM is uploaded to the system
When I send a request to the OCM service to evaluate policy
Then I get a successful response
And the response contains the link to the QR code
And the response contains the presentationID

Scenario: Make request for getting claims by presentationID
Given the policy to query OCM is uploaded to the system
Given the request to the OCM service 
When I send a request to the OCM service
And the request contains presentationID
Then I get a successful response
And response contains requested claims