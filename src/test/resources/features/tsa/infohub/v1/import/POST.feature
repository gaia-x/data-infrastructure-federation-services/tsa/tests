#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/tsa/infohub/v1/import
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @infohub
Feature: API - TSA - Infohub - v1/export POST
  As user
  I want to import data in to Infohub
  So I can export it later

  Background:
    Given we are testing the TSA Infohub Api

  Scenario: TSA - Import data to Infohub - Positive
    When I load the REST request {Infohub.json} with profile {successful_import}
    And I import data via TSA Infohub API
    Then the status code should be {200}
    And the response is valid according to the {Infohub_Import_schema.json} REST schema

  Scenario: TSA - Import data with ID to Infohub - Positive
    When I load the REST request {Infohub.json} with profile {successful_import_ID}
    And I import data via TSA Infohub API
    Then the status code should be {200}
    And the response is valid according to the {Infohub_Import_schema.json} REST schema

  @negative
  Scenario: TSA - Import data with modified ID to Infohub - Negative
    When I load the REST request {Infohub.json} with profile {modified_ID}
    And I import data via TSA Infohub API
    Then the status code should be {400}

  @negative
  Scenario: TSA - Import data with modified ID to Infohub - Negative
    When I load the REST request {Infohub.json} with profile {modified_allow}
    And I import data via TSA Infohub API
    Then the status code should be {400}

  @negative
  Scenario: TSA - Import empty data to Infohub - Negative
    When I set the following request body {{}}
    And I import data via TSA Infohub API
    And the status code should be {400}