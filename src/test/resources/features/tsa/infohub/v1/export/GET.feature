#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/tsa/infohub/v1/export
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @infohub
Feature: API - TSA - Infohub - v1/export GET
  As user
  I want to export data wrapped as Verifiable Credential or Presentation
  So I can have the data with proof

  Background:
    Given we are testing the TSA Infohub Api

  Scenario: TSA - Export through Infohub - Positive
    When I export the {testexport} via TSA Infohub API
    Then the status code should be {200}
    ## If the result is not in the cache the first call returns the below result
    ## And the field {result} has the value {export request is accepted}
    When I export the {testexport} via TSA Infohub API
    Then the status code should be {200}
    And the response is valid according to the {Infohub_Export_schema.json} REST schema
    And the field {proof.verificationMethod} contains the value {vereign.com:tsa:policy:policy:example:returnDID:1.0:evaluation#key1}
    And the field {proof.type} has the value {JsonWebSignature2020}

  @negative
  Scenario: TSA - Export missing export through Infohub - Negative
    When I export the {missing_export} via TSA Infohub API
    Then the status code should be {404}
    And the response is valid according to the {Policy_Evaluate_negative_schema.json} REST schema
    And the field {message} has the value {export configuration not found}
