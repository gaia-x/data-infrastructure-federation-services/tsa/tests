#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#http://gaiax.vereign.com/tsa/task/v1/taskList
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @task
Feature: API -TSA - Task - v1/taskList POST
  As user
  I want to execute the list of tasks
  So I am able to bring the consistency to running multiple asynchronous tasks

  Background:
    Given we are testing the TSA Task Api

  Scenario: TSA - Executing Task List with two synchronous tasks - Positive
    When I load the REST request {Policy.json} with profile {did_key}
    And I execute the taskList {testList} via TSA Task API
    Then the status code should be {200}
    And the response is valid according to the {TaskList_Execute_schema.json} REST schema