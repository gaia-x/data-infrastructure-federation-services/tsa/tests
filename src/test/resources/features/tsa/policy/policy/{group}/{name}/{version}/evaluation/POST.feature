#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/tsa/policy/policy/:group/:name/:version/evaluation
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @policy
Feature: API -TSA - Policy - :group/:name/:version/evaluation POST
  As user
  I want to evaluate the policy
  So I am able to execute it in the future

  Acceptance criteria:
  - HTTP endpoint to evaluate the policy
  - example policy created and committed to Git repo
  - Green test based on example committed to the system

  Background:
    Given we are testing the TSA Policy Api

  Scenario: TSA - Evaluate policy synchronously - Positive
    When I load the REST request {Policy.json} with profile {successful_message}
    And I execute the Policy group {example} name {test} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the response is valid according to the {Policy_Evaluate_schema.json} REST schema
    And the field {allow} has the value {true}

  Scenario: TSA - DID resolution - Positive
    When I load the REST request {Policy.json} with profile {did_key}
    And I execute the Policy group {example} name {resolve} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the response is valid according to the {Policy_EvaluateDID_schema.json} REST schema
    And the field {data.didDocument.id} has the value {did:key:z6Mkfriq1MqLBoPWecGoDLjguo1sB9brj6wT3qZ5BxkKpuP6}

  Scenario: TSA - using header values for policy execution - Positive
    When I load the REST request {Policy.json} with profile {empty}
    And I load value {someValue} into current request HEADER {X-Cache-Key}
    And I execute the Policy group {example} name {getHeader} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the field {key} has the value {someValue}

  Scenario: TSA - using multiple header values for policy execution - Positive
    When I load the REST request {Policy.json} with profile {empty}
    And I load value {someValue} into current request HEADER {X-Cache-Key}
    And I load value {another-value} into current request HEADER {X-Another-Header}
    And I execute the Policy group {example} name {getMultiHeaders} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the field {key} has the value {someValue}
    And the field {another} has the value {another-value}

  @negative
  Scenario: TSA - Evaluate policy with incorrect value - Negative
    When I load the REST request {Policy.json} with profile {incorrect_message}
    And I execute the Policy group {example} name {test} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the response is valid according to the {Policy_Evaluate_schema.json} REST schema
    And the field {allow} has the value {false}

  @negative
  Scenario: TSA - Evaluate policy with missing body- Negative
    And I execute the Policy group {example} name {test} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the response is valid according to the {Policy_Evaluate_schema.json} REST schema
    And the field {allow} has the value {false}

  @negative
  Scenario: TSA - Evaluate missing policy - Negative
    When I load the REST request {Policy.json} with profile {successful_message}
    And I execute the Policy group {example} name {missing} version {1.0} via TSA Policy API
    Then the status code should be {404}
    And the response is valid according to the {Policy_Evaluate_negative_schema.json} REST schema
    And the field {message} has the value {error evaluating policy}

  @negative
  Scenario: TSA - DID resolution with incorrect did - Negative
    When I load the REST request {Policy.json} with profile {did_missing_method}
    And I execute the Policy group {example} name {resolve} version {1.0} via TSA Policy API
    Then the status code should be {200}
    And the response is valid according to the {Policy_EvaluateDID_negative_schema.json} REST schema
    And the field {data.didResolutionMetadata.error} has the value {notFound}
