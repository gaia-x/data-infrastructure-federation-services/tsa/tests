#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/policy/policy/:group/:name/:version/lock
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @policy @lock
Feature: API - TSA - Policy - :group/:name/:version/lock DELETE
  As user
  I want to unlock a policy
  So I can evaluate it

  Background:
    Given we are testing the TSA Policy Api

  Scenario: TSA - Unlock policy - Positive
    When I lock the Policy group {example} name {test} version {1.0}
    And I unlock the policy group {example} name {test} version {1.0}
    And the status code should be {200}
    # Check if the policy can be executed
    When I load the REST request {Policy.json} with profile {successful_message}
    And I execute the Policy group {example} name {test} version {1.0} via TSA Policy API
    Then the status code should be {200}

  @negative
  Scenario: TSA - Unlock none existing policy - Negative
    And I unlock the policy group {example} name {non_existing} version {1.0}
    And the status code should be {404}
    And the field {message} has the value {policy not found}
