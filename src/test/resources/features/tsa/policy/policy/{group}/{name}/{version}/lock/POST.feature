#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/policy/policy/:group/:name/:version/lock
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @policy @lock
Feature: API - TSA -Policy - :group/:name/:version/lock POST
  As user
  I want to evaluate the policy
  So I am able to execute it in the future

  Acceptance criteria:
  - HTTP endpoint to evaluate the policy
  - example policy created and committed to Git repo
  - Green test based on example committed to the system

  Background:
    Given we are testing the TSA Policy Api

  Scenario: TSA - Lock policy - Positive
    When I lock the Policy group {example} name {test} version {1.0}
    Then the status code should be {200}
    When I load the REST request {Policy.json} with profile {successful_message}
    And I execute the Policy group {example} name {test} version {1.0} via TSA Policy API
    Then the status code should be {403}
    And the field {message} has the value {error evaluating policy}
    And I unlock the policy group {example} name {test} version {1.0}

  @negative
  Scenario: TSA - Lock already locked policy - Negative
    When I lock the Policy group {example} name {test} version {1.0}
    When I lock the Policy group {example} name {test} version {1.0}
    And the status code should be {403}
    And the field {message} has the value {policy is already locked}
    And I unlock the policy group {example} name {test} version {1.0}
