#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/tsa/signer/v1/credential/proof
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @signer
Feature: API - TSA - Signer credential proof - v1/credential/proof POST
  As user
  I want to create a credential proof
  So the credential can be verified

  Background:
    Given we are testing the TSA Signer Api

  Scenario Outline: TSA - create credential proof <labelSuffix> - Positive
    When I load the REST request {Credential.json} with profile {<profileOption>}
    And I create credential proof via TSA Signer API
    Then the status code should be {200}
    And the response is valid according to the {<schema>} REST schema
    And the field {proof.verificationMethod} has the value {did:web:gaiax.vereign.com:tsa:policy:policy:example:returnDID:1.0:evaluation#key1}

    Examples:
      | labelSuffix | profileOption        | schema                                        |
      |             | for_proof            | Signer_CredentialProof_schema.json            |
      | alumni of   | for_proof_alumni     | Signer_CredentialProof_alumni_schema.json     |
      | without ID  | for_proof_without_ID | Signer_CredentialProof_without_ID_schema.json |

  @negative
  Scenario: TSA - create credential proof with empty body - Negative
    When I set the following request body {{}}
    And I create credential proof via TSA Signer API
    Then the status code should be {400}
    And the field {message} contains the value {"namespace" is missing from body}
    And the field {message} contains the value {"key" is missing from body}
    And the field {message} contains the value {"credential" is missing from body}

  @negative
  Scenario Outline: TSA - create credential proof with incorrect ID (<labelSuffix>)- Negative
    When I load the REST request {Credential.json} with profile {<profileOption>}
    And I create credential proof via TSA Signer API
    Then the status code should be {400}
    And the field {message} has the value {invalid format of subject id}

    Examples:
      | labelSuffix | profileOption                  |
      | space       | for_proof_incorrect_ID_space   |
      | symbols     | for_proof_incorrect_ID_symbols |