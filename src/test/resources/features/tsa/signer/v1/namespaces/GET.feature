#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/tsa/signer/v1/namespaces
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @signer
Feature: API - TSA - Signer key namespaces - v1/namespaces GET
  As user
  I want to see what key namespaces are available
  So I am able to use the keys in the namespace in the future

  Background:
    Given we are testing the TSA Signer Api

  Scenario: TSA - Getting all key namespaces from Singer - Positive
    When I get all key namespaces via TSA Signer API
    And the status code should be {200}
    And the response body contains {transit}