#Copyright (c) 2018 Vereign AG [https://www.vereign.com]
#
#This is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#https://gaiax.vereign.com/tsa/signer/v1/namespaces/{namespace}/keys
#Author: Georgi Michev georgi.michev@vereign.com

@rest @all @tsa @signer
Feature: API - TSA - Signer key namespaces - v1/namespaces/{namespace}/keys GET
  As user
  I want to see what keys a namespaces have
  So I am able to use the keys in them in the future

  Background:
    Given we are testing the TSA Signer Api

  Scenario: TSA - Getting all keys from specific namespace Singer - Positive
    When I get all keys from namespace {transit} via TSA Signer API
    And the status code should be {200}
    And the response body contains {key1}

  @Negative
  Scenario: TSA - Getting keys from non existing namespace Singer - Negative
    When I get all keys from namespace {non_existing} via TSA Signer API
    And the status code should be {404}
    And the field {message} contains the value {no keys found in namespace}