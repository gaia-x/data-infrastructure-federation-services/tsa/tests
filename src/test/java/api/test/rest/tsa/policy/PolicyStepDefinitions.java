/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest.tsa.policy;

import api.test.core.BaseStepDefinitions;
import api.test.rest.RestGeneralStepDefinitions;
import api.test.rest.RestSessionContainer;
import core.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PolicyStepDefinitions extends BaseStepDefinitions {

    private static final Logger logger = LogManager.getLogger(RestGeneralStepDefinitions.class.getSimpleName());
    RestSessionContainer restSessionContainer;
    Request currentRequest;

    public PolicyStepDefinitions(RestSessionContainer restSessionContainer, Request currentRequest, DataContainer dataContainer) {
        super(dataContainer);
        this.restSessionContainer = restSessionContainer;
        this.currentRequest = currentRequest;
    }

    @And("^I execute the Policy group \\{(.*)\\} name \\{(.*)\\} version \\{(.*)\\} via TSA Policy API$")
    public void iExecuteThePolicyGroupNameVersionTSAPolicyAPI(String group, String name, String version) throws Throwable {
        currentRequest.setPath("/policy/" + group + "/" + name + "/" + version + "/evaluation");
        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @When("^I lock the Policy group \\{(.*)\\} name \\{(.*)\\} version \\{(.*)\\}$")
    public void iLockThePolicyGroupNameVersion(String group, String name, String version) throws Throwable {
        currentRequest.setPath("/policy/" + group + "/" + name + "/" + version + "/lock");
        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

    @When("^I unlock the policy group \\{(.*)\\} name \\{(.*)\\} version \\{(.*)\\}$")
    public void iUnlockThePolicy(String group, String name, String version) throws Throwable {
        currentRequest.setPath("/policy/" + group + "/" + name + "/" + version + "/lock");
        Response response = RestClient.delete(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }
}