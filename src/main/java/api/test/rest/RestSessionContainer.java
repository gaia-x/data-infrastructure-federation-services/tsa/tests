/*
Copyright (c) 2018 Vereign AG [https://www.vereign.com]

This is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package api.test.rest;

import java.util.List;

public class RestSessionContainer {
    private String taskID;
    private String taskListID;
    private List<String> groupTaskIDs;
    private List<String> importIDs;

    public List<String> getImportIDs() {
        return importIDs;
    }

    public void setImportIDs(List<String> importIDs) {
        this.importIDs = importIDs;
    }

    public List<String> getGroupTaskIDs() {
        return groupTaskIDs;
    }

    public void setGroupTaskIDs(List<String> groupTaskIDs) {
        this.groupTaskIDs = groupTaskIDs;
    }

    public String getTaskID() {
        return taskID;
    }

    public String getTaskListID() {
        return taskListID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public void setTaskListID(String taskListID) {
        this.taskListID= taskListID;
    }
}