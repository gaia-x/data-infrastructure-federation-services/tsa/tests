$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/batch/api/getStatuses/POST.feature");
formatter.feature({
  "name": "API - getStatuses POST",
  "description": "  Get the previously added Batches",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@batch"
    },
    {
      "name": "@all"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "we are testing the VIAM Api",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.we_are_testing_the_VIAM_api()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Send a batch request and then fetch it with getStatuses - Positive",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@rest"
    },
    {
      "name": "@batch"
    },
    {
      "name": "@all"
    },
    {
      "name": "@getStatuses"
    },
    {
      "name": "@test"
    }
  ]
});
formatter.step({
  "name": "I load the REST request {Batch.json} with profile {successful_batch}",
  "keyword": "Given "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.I_load_the_REST_request__with_profile_(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I send a new random batch request via API",
  "keyword": "Given "
});
formatter.match({
  "location": "BatchStepDefinitions.I_send_a_new_random_batch_request_via_API()"
});
formatter.result({
  "error_message": "java.net.UnknownHostException: batch-service.rse-test.k8s.vereign.com: Name or service not known\n\tat java.base/java.net.Inet4AddressImpl.lookupAllHostAddr(Native Method)\n\tat java.base/java.net.InetAddress$PlatformNameService.lookupAllHostAddr(InetAddress.java:933)\n\tat java.base/java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1519)\n\tat java.base/java.net.InetAddress$NameServiceAddresses.get(InetAddress.java:852)\n\tat java.base/java.net.InetAddress.getAllByName0(InetAddress.java:1509)\n\tat java.base/java.net.InetAddress.getAllByName(InetAddress.java:1367)\n\tat java.base/java.net.InetAddress.getAllByName(InetAddress.java:1301)\n\tat org.apache.http.impl.conn.SystemDefaultDnsResolver.resolve(SystemDefaultDnsResolver.java:45)\n\tat org.apache.http.impl.conn.DefaultClientConnectionOperator.resolveHostname(DefaultClientConnectionOperator.java:263)\n\tat org.apache.http.impl.conn.DefaultClientConnectionOperator.openConnection(DefaultClientConnectionOperator.java:162)\n\tat org.apache.http.impl.conn.ManagedClientConnectionImpl.open(ManagedClientConnectionImpl.java:326)\n\tat org.apache.http.impl.client.DefaultRequestDirector.tryConnect(DefaultRequestDirector.java:605)\n\tat org.apache.http.impl.client.DefaultRequestDirector.execute(DefaultRequestDirector.java:440)\n\tat org.apache.http.impl.client.AbstractHttpClient.doExecute(AbstractHttpClient.java:835)\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:83)\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:56)\n\tat org.apache.http.client.HttpClient$execute$0.call(Unknown Source)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\n\tat io.restassured.internal.RequestSpecificationImpl$RestAssuredHttpBuilder.doRequest(RequestSpecificationImpl.groovy:2088)\n\tat io.restassured.internal.http.HTTPBuilder.post(HTTPBuilder.java:350)\n\tat io.restassured.internal.http.HTTPBuilder$post$2.call(Unknown Source)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\n\tat io.restassured.internal.RequestSpecificationImpl.sendRequest(RequestSpecificationImpl.groovy:1194)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:77)\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:568)\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1268)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1035)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:819)\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:166)\n\tat io.restassured.internal.filter.SendRequestFilter.filter(SendRequestFilter.groovy:30)\n\tat io.restassured.filter.Filter$filter$0.call(Unknown Source)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\n\tat io.restassured.filter.time.TimingFilter.filter(TimingFilter.java:56)\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\n\tat io.restassured.filter.log.RequestLoggingFilter.filter(RequestLoggingFilter.java:146)\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:157)\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\n\tat io.restassured.filter.FilterContext$next.call(Unknown Source)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\n\tat io.restassured.internal.RequestSpecificationImpl.applyPathParamsAndSendRequest(RequestSpecificationImpl.groovy:1686)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:77)\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:568)\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1268)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1035)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:819)\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:203)\n\tat io.restassured.internal.RequestSpecificationImpl.applyPathParamsAndSendRequest(RequestSpecificationImpl.groovy:1692)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:77)\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:568)\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1268)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1035)\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:819)\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:203)\n\tat io.restassured.internal.RequestSpecificationImpl.post(RequestSpecificationImpl.groovy:180)\n\tat io.restassured.internal.RequestSpecificationImpl.post(RequestSpecificationImpl.groovy)\n\tat core.RestClient.post(RestClient.java:156)\n\tat api.test.rest.batch.BatchStepDefinitions.I_send_a_new_batch_request_via_API(BatchStepDefinitions.java:53)\n\tat api.test.rest.batch.BatchStepDefinitions.I_send_a_new_random_batch_request_via_API(BatchStepDefinitions.java:118)\n\tat ✽.I send a new random batch request via API(file:src/test/resources/features/batch/api/getStatuses/POST.feature:30)\n",
  "status": "failed"
});
formatter.step({
  "name": "the status code should be {200}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I wait for {60000} mseconds",
  "keyword": "Then "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_wait_for_mseconds(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I clear the request body",
  "keyword": "Given "
});
formatter.match({
  "location": "GeneralStepDefinitions.I_clear_the_request_body()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I call getStatuses with the current sealKey via API",
  "keyword": "Then "
});
formatter.match({
  "location": "BatchStepDefinitions.I_call_getStatuses_with_the_current_sealKey_via_API(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the status code should be {200}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_status_code_should_be(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the field {$.status} has the value {OK}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_has_the_value_(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the field {$.code} has the value {200}",
  "keyword": "And "
});
formatter.match({
  "location": "GeneralStepDefinitions.the_field_has_the_value_(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the response is valid according to the {Batch_GetStatuses_schema.json} REST schema",
  "keyword": "And "
});
formatter.match({
  "location": "RestGeneralStepDefinitions.the_response_is_valid_according_to_the_REST_schema(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I assert that getStatus status is the same as the sendBatch value via API",
  "keyword": "And "
});
formatter.match({
  "location": "BatchStepDefinitions.I_assert_that_getStatus_status_is_the_same_as_the_sendBatch_value_via_API()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});
